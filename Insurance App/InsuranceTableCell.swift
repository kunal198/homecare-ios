//
//  InsuranceTableCell.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class InsuranceTableCell: UITableViewCell {

    @IBOutlet var companyname: UIImageView!
    
    @IBOutlet var insurancename: UILabel!
    
    @IBOutlet var yearly: UILabel!
    
    @IBOutlet var euro: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
