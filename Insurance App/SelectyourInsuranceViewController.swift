//
//  SelectyourInsuranceViewController.swift
//  Insurance App
//
//  Created by brst on 18/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class SelectyourInsuranceViewController: UIViewController,UITextFieldDelegate
{
     var dateFormatter = NSDateFormatter()
    @IBOutlet var selectmalebtn: UIButton!
    var checkbtn = Bool()
    var isMale = true
    var isFemale =  true
    
    
    @IBOutlet var date: UIDatePicker!
    @IBOutlet var femalebtn: UIButton!
    
    @IBOutlet var maleblankbtn: UIButton!
    @IBOutlet var nametext: UITextField!
    
    @IBOutlet var lastname: UITextField!
    
    
    @IBOutlet var dateofbirth: UITextField!
    
    
    @IBOutlet var adress: UITextField!
    
    @IBOutlet var email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkbtn = false
//        if checkbtn == false
//        {
//        self.femaleselectbtn.hidden = true
//        self.malebtn.hidden = true
//        }
       self.dateofbirth.delegate = self
       self.adress.delegate = self
       self.email.delegate = self
        
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        
        self.email .resignFirstResponder()
        self.adress .resignFirstResponder()
        self.dateofbirth .resignFirstResponder()
        self.lastname .resignFirstResponder()
        self.nametext .resignFirstResponder()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func pickdate(sender: AnyObject)
    {

        

        
    }
    

    @IBAction func backbtn(sender: AnyObject)
    
    {
        
       self.navigationController?.popViewControllerAnimated(false)
        
    }
    
    
    @IBAction func malebtn(sender: AnyObject)
    {
        self.selectmalebtn.hidden = false
        if isMale == true
        {
            
            maleblankbtn.setImage(UIImage(named: "Confirm_code_icon.png"), forState: UIControlState.Normal)
            femalebtn.setImage(UIImage(named: "un_select_icon.png"), forState: UIControlState.Normal)
            isFemale = true
            isMale = false

        
         }
        else
        {
            maleblankbtn.setImage(UIImage(named: "un_select_icon.png"), forState: UIControlState.Normal)
            isFemale = false
            isMale = true

        }
    }
    
    @IBAction func femalebtn(sender: AnyObject)
    {
        self.selectmalebtn.hidden = true
        
        if isFemale == true
        {
            
            femalebtn.setImage(UIImage(named: "Confirm_code_icon.png"), forState: UIControlState.Normal)
            maleblankbtn.setImage(UIImage(named: "un_select_icon.png"), forState: UIControlState.Normal)
            isMale = true
            isFemale = false

            
        }
        else
        {
            femalebtn.setImage(UIImage(named: "un_select_icon.png"), forState: UIControlState.Normal)
            isMale = false
            isFemale = true
          
        }
    
  

        
    }
   
    @IBAction func continuebtn(sender: AnyObject)
    {
        
        
        if (self.nametext.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie den Namen"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
        else if (self.lastname.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie die Nachname"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
        
        else if (self.dateofbirth.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie die dateOfBirth"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
        else if (self.adress.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie die Adresse"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
        else if (self.email.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie die E-Mail"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }

        else if isValidEmail(email.text!) == false{
            //your code here
        
        
            
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie die richtige E-Mail -Adresse"
            alert.addButtonWithTitle("Ok")
            alert.show()
        
        }
        
        else
        {
        
        
        let NewInsurance = self.storyboard!.instantiateViewControllerWithIdentifier("SelectyourinsuranceConfirmViewController") as! SelectyourinsuranceConfirmViewController
        
        self.navigationController?.pushViewController(NewInsurance, animated: false)
        
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = testStr.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? true : false
        return result
        
    }
    
    
        
        
    
    
    @IBAction func resigntextfield(sender: AnyObject)
    {
        
        sender .resignFirstResponder()
    }
    
    
    
    
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        
        
        if textField == dateofbirth
        {
            //self.date.hidden = false
            datePickerOpen()
        }
        
        
    if textField == adress
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, -120, self.view.frame.size.width, self.view.frame.size.height)
    }
    
        if textField == email
        {
             self.view.frame = CGRectMake(self.view.frame.origin.x, -150, self.view.frame.size.width, self.view.frame.size.height)
        }
//        if adress.tag == 2
//        {
//            
//            
////            self.date.hidden = true
//        }
//        
//        if email.tag == 3
//        {
//            
//           
////            self.date.hidden = true
//        }
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if textField == dateofbirth
        {
           // self.date.hidden = true
            
        }
        else
        {
            //self.date.hidden = true
        }
        
        if dateofbirth.tag == 1
        {
            //self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)
            
//            var dateFormat = NSDateFormatter()
//            var strFormat = NSString()
//            
//            dateFormat.dateFormat = "MM-dd-yyyy"
//            
//            strFormat = dateFormat.stringFromDate(date.date)
//            
//            
//            dateofbirth.text = strFormat as String
            
            
           // dateofbirth.resignFirstResponder()
            
            
            
            
            
            
            if dateofbirth.text != ""
            {
               //   self.date.hidden = true
                
            }

        }
        
        if adress.tag == 2
        {
            
            
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)
        }
        
        if email.tag == 3
        {
            
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)
        }
        
        
    }

    
    
    func datePickerOpen()
    {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        dateofbirth.inputView = datePickerView
        
        datePickerView.addTarget(self, action: Selector("dataPickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        //datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        
        // dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = -116
        let minDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        
        components.year = 0
        let maxDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        
        
    }
    
    
    func dataPickerChanged(datePickerView:UIDatePicker)
    {
        dateFormatter.dateFormat = "MMM dd,yyyy"
        
        let strDate = dateFormatter.stringFromDate(datePickerView.date)
        self.dateofbirth.text = strDate
        
        //        let dateFormatter = NSDateFormatter()
        //        //dateFormatter.dateFormat = "MM-dd-yyyy"
        //        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        //
        //        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        //
        //        dateofbirth.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    
}
