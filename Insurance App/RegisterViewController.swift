//
//  RegisterViewController.swift
//  Insurance App
//
//  Created by brst on 23/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    var dataModel = NSData()
    
    var data = NSData()
    @IBOutlet var mobilenotext: UITextField!
    @IBOutlet var nametext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.mobilenotext .resignFirstResponder()
    }
    
    @IBAction func resign(sender: AnyObject)
    {
        sender .resignFirstResponder()
    }
    
    @IBAction func `continue`(sender: AnyObject)
    {
        if (self.nametext.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Message"
            alert.message = "Please enter Name"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
        else if (self.mobilenotext.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Message"
            alert.message = "Please enter Mobile Number"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
            
        else if (self.mobilenotext.text!.characters.count <= 9)
        {
            let alert = UIAlertView()
            alert.title = "Message"
            alert.message = "Please enter 10 digit Mobile Number"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
            
        else
        {
            
            let post = NSString(format:"name=%@&mobileno=%@",nametext.text!,mobilenotext.text!)
            
            
            //println(post)
            
            dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            let postLength = String(dataModel.length)
            
            let url = NSURL(string: "http://beta.brstdev.com/insurance/backend/index.php/login/register")
            
            let urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
                
                do
                {
                    let dicObj = try NSJSONSerialization.JSONObjectWithData(self.data, options: NSJSONReadingOptions()) as? NSDictionary
                    print(dicObj)
                    
                }
                    
                catch let error as NSError
                {
                    print(error.localizedDescription)
                }
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        
                    })
                    
                }
                    
                else
                {
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        
                        //var success = dicObj?.valueForKey("success") as! Bool
                        
                        //                        if success == false
                        //                        {
                        //                            var dict = dicObj?.valueForKey("message") as! NSDictionary
                        //                            var message = dict.valueForKey("Email") as! String
                        //                            var alert = UIAlertView()
                        //                            alert = UIAlertView(title:"Alert", message: message, delegate: self, cancelButtonTitle:"OK")
                        //                            alert.show()
                        //
                        //                        }
                        //                        else
                        //
                        //                        {
                        //                            var messageSuccessfully = dicObj?.valueForKey("message") as! String
                        //
                        //
                        //                            self.alertSuccessFull_regis = UIAlertView(title:"Alert", message: messageSuccessfully, delegate: self, cancelButtonTitle:"OK")
                        //
                        //                            self.alertSuccessFull_regis.show()
                        //
                        //
                        //
                        //                        }
                        
                        
                        
                        
                    })
                    
                    
                }
                
                
            })
            task.resume()
            
            
            let Terms = self.storyboard!.instantiateViewControllerWithIdentifier("RegistercodeViewController") as! RegistercodeViewController
            
            self.navigationController?.pushViewController(Terms, animated: true)
            
        }

        
    }
    
    @IBAction func termsbtn(sender: AnyObject)
    {
       
        let Terms = self.storyboard!.instantiateViewControllerWithIdentifier("TermsViewController") as! TermsViewController
        
        self.navigationController?.pushViewController(Terms, animated: true)
        
    }
    
    
    @IBAction func privacypolicybtn(sender: AnyObject)
    {
        let PrivacyPolicy = self.storyboard!.instantiateViewControllerWithIdentifier("PrivacyPolicyViewController") as! PrivacyPolicyViewController
        
        self.navigationController?.pushViewController(PrivacyPolicy, animated: true)
        
    }

    
    
    
}
