//
//  InsuranceViewController.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class InsuranceViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
//    var myMenuView :UIView = UIView()
    @IBOutlet var Insurancetable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        myMenuView.hidden = true
        self.Insurancetable.delegate = self
        currentVC = self
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width        // Do any additional setup after loading the view.
    }
    override func prefersStatusBarHidden() -> Bool
    {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }
    override func viewWillAppear(animated: Bool) {
        myMenuView.hidden = true
        closeMenuBtn.hidden = true
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menubar(sender: AnyObject)
    {
         myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
//            x = -myMenuView.frame.size.width
//            navx = 0
            closeMenuBtn.hidden = false
        }
        
        
        UIView.animateWithDuration(0.5, animations: {
            
            // self.navBar.frame.origin.x = self.navx
            myMenuView.frame.origin.x = self.x
            
        })
    
    }

    @IBAction func addbtn(sender: AnyObject)
    {
        
        let SelectInsurance = self.storyboard!.instantiateViewControllerWithIdentifier("SelectInsuranceViewController") as! SelectInsuranceViewController
        
        self.navigationController?.pushViewController(SelectInsurance, animated: true)

    }
    
    func tableView(tableView: UITableView, numberOfSectionsInTable sections:Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! InsuranceTableCell
        
        
    
        
        return cell
        
    }
    
    
    
    
}
