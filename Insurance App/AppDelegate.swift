//
//  AppDelegate.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

var x: CGFloat = 0.0
var urlLink = String()

var navBarTitle = String()

var currentVC:UIViewController = UIViewController()

var postPropertyVC: UIViewController = UIViewController()

var myMenuView :UIView = UIView()
var closeMenuBtn:UIButton = UIButton()

var barButtonCheck = String()

let dashBoard_btn:UIButton = UIButton()
let advancedSearch_btn:UIButton = UIButton()
let propertyForSale_btn:UIButton = UIButton()
let propertyForRent_btn:UIButton = UIButton()
let postProperty_btn:UIButton = UIButton()


let contactUS_btn:UIButton = UIButton()


let aboutUs_Btn:UIButton = UIButton()
let feedBack_btn:UIButton = UIButton()




var dashBoard_Image : UIImageView = UIImageView()
var advancedSearch_Image : UIImageView = UIImageView()
var propertyForSale_Image : UIImageView = UIImageView()
var propertyForRent_Image : UIImageView = UIImageView()
var postProperty_Image : UIImageView = UIImageView()
var aboutUs_Image : UIImageView = UIImageView()
var feedBack_Image : UIImageView = UIImageView()

var contactUS_Image : UIImageView = UIImageView()


let dashBoardLAbel:UILabel = UILabel()
let advancedSearchLabel:UILabel = UILabel()
let propertyForSaleLabel:UILabel = UILabel()
let propertyForRentLabel:UILabel = UILabel()
let postPropertyLabel:UILabel = UILabel()
let aboutUsLabel:UILabel = UILabel()
let feedBackLabel:UILabel = UILabel()

let contactUSLabel:UILabel = UILabel()




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var menu_Label:UILabel = UILabel()
    
    let lineImage: UIImageView = UIImageView()
    
    
    //MARK: 1.DashBoard View Variables
    var dashBoardView :UIView = UIView()
    var dashBoardView_line:UILabel = UILabel()
    
    
    //MARK: 2.About View Variables
    var advancedSearchView :UIView?
    var advanceSearchView_line:UILabel = UILabel()
    
    
    
    //MARK: 4.Resale Properties View Variables
    var propertyForSaleView :UIView?
    var propertyForSaleView_line:UILabel = UILabel()
    
    
    
    //MARK: 5.Exchange Property View Variables
    var propertyForRentView :UIView?
    var propertyForRentView_line:UILabel = UILabel()
    
    
    //MARK: 6.Post Property View Variables
    var postPropertyView :UIView?
    var postPropertyView_line:UILabel = UILabel()
    
    
    
    //MARK: 7.Contact Us View Variables
    var  aboutUsView:UIView?
    var aboutUsView_line:UILabel = UILabel()
    
    
    
    //MARK: 7.Contact Us View Variables
    var  feedBackView:UIView?
    var feedBackView_line:UILabel = UILabel()
    
    
    
    //MARK: 9.Contact US View Variables
    var  contactUSView:UIView?
    var contactUSView_line:UILabel = UILabel()
    
    
   
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().statusBarHidden = true
        
        creatingMenu()
        return true
    }

    func creatingMenu()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            myMenuView = UIView(frame:CGRectMake(0, 64,(250/320)*UIScreen.mainScreen().bounds.size.width, (800/568)*UIScreen.mainScreen().bounds.size.height))
            
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                myMenuView = UIView(frame:CGRectMake(0, 0, 250, 568))
                
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
                myMenuView = UIView(frame: CGRectMake(0, 0, 250, 800))
                
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                myMenuView = UIView(frame: CGRectMake(0, 0, 350, 800))
            }
                
            else
            {
                myMenuView = UIView(frame:CGRectMake(0, 0,320 , 800))
            }
            
            
        }
        
        closeMenuBtn.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height)
        closeMenuBtn.backgroundColor = UIColor.blackColor()
        closeMenuBtn.alpha = 0.5
        closeMenuBtn.hidden = true
        
        window?.addSubview(closeMenuBtn)
        window?.addSubview(myMenuView)
        
        myMenuView.backgroundColor = UIColor.whiteColor()
        
        
        closeMenuBtn.addTarget(self, action:"closeMenuBtnClicked:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        
        
        
        dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height))
        
        dashBoardView.backgroundColor =  UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        
        
        
        
        
        myMenuView .addSubview(dashBoardView)
        
        
        dashBoardLAbel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height,(120/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoardLAbel.textColor = UIColor.whiteColor()
        dashBoardLAbel.textAlignment = NSTextAlignment.Left
        dashBoardLAbel.text = "MENU"
        dashBoardLAbel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        dashBoardLAbel.tag = 0
        
        myMenuView .addSubview(dashBoardLAbel)
        
        
        // homeLabel.backgroundColor = UIColor.whiteColor()
        
        
        //        dashBoard_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (18/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        //        dashBoard_Image.image = UIImage(named:"contact_icon.png")
        //
        //
        //        dashBoard_btn .addSubview(dashBoard_Image)
        
        
        
        
        //        dashBoard_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        dashBoard_btn.backgroundColor = UIColor.whiteColor()
        //
        //        dashBoard_btn.setTitle ("", forState: UIControlState.Normal)
        //        dashBoard_btn.addTarget(self, action:"goTDashBoardVC:",forControlEvents: UIControlEvents.TouchUpInside)
        //
        //        dashBoardView.addSubview(dashBoard_btn)
        //
        //        dashBoard_btn.tag = 0
        
        //dashBoard_btn.backgroundColor = UIColor.blackColor()
        
        
        dashBoardView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(62/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(3/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoardView_line.backgroundColor = UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        
        
        myMenuView.addSubview(dashBoardView_line)
        //myEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //dashBoardLAbel.textColor = UIColor.blackColor()
        //dashBoardLAbel.textAlignment = NSTextAlignment.Left
        //dashBoardLAbel.text = "Dashboard"
        //dashBoardLAbel.font = UIFont.boldSystemFontOfSize(13)
        
        
        
        
        //MARK: - 2.About View
        advancedSearchView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        advancedSearchView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(advancedSearchView!)
        
        
        advancedSearch_Image = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (18/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        advancedSearch_Image.image = UIImage(named:"contact_icon.png")
        
        advancedSearch_btn.addSubview(advancedSearch_Image)
        
        
        
        advancedSearchLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        advancedSearchLabel.textColor = UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        advancedSearchLabel.textAlignment = NSTextAlignment.Left
        advancedSearchLabel.text = "Kontakt"
        advancedSearchLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        
        advancedSearchLabel.tag = 1
        
        
        advancedSearch_btn.addSubview(advancedSearchLabel)
        
        
        
        advancedSearch_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height)
        
        advancedSearch_btn.backgroundColor = UIColor.whiteColor()
        advancedSearch_btn.setTitle ("", forState: UIControlState.Normal)
        advancedSearch_btn.addTarget(self, action:"goToAdvancedSearchVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        // advancedSearch_btn.backgroundColor = UIColor.blackColor()
        
        advancedSearchView!.tag = 95
        
        advancedSearchView?.addSubview(advancedSearch_btn)
        
        advancedSearch_btn.tag = 1
        
        
        
        advanceSearchView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(124/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        advanceSearchView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(advanceSearchView_line)
        
        
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            advancedSearchLabel.font = UIFont.boldSystemFontOfSize(18)
        //            advanceSearchView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            advancedSearchLabel.font = UIFont.boldSystemFontOfSize(13)
        //            advanceSearchView_line.frame.size.height = 1
        //        }
        
        
        
        //MARK: - 3.Mega Projects View
        propertyForSaleView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (130/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForSaleView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(propertyForSaleView!)
        
        
        
        propertyForSale_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width,(12/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForSale_Image.image = UIImage(named:"imprint_icon.png")
        
        propertyForSale_btn.addSubview(propertyForSale_Image)
        
        
        
        propertyForSale_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForSale_btn.backgroundColor = UIColor.whiteColor()
        
        propertyForSale_btn.setTitle ("", forState: UIControlState.Normal)
        propertyForSale_btn.addTarget(self, action:"goToPropertiesForSaleVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        propertyForSaleView?.addSubview(propertyForSale_btn)
        
        propertyForSale_btn.tag = 2
        
        
        
        propertyForSaleLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(15/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        propertyForSaleLabel.textColor = UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        propertyForSaleLabel.textAlignment = NSTextAlignment.Left
        propertyForSaleLabel.text = "Impressum"
        propertyForSaleLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        
        
        propertyForSale_btn.addSubview(propertyForSaleLabel)
        
        
        propertyForSale_btn.tag = 2
        
        
        
        
        propertyForSaleView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(186/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForSaleView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(propertyForSaleView_line)
        
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            propertyForSaleLabel.font = UIFont.boldSystemFontOfSize(18)
        //            propertyForSaleView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            propertyForSaleLabel.font = UIFont.boldSystemFontOfSize(13)
        //            propertyForSaleView_line.frame.size.height = 1
        //        }
        
        
        
        
        
        
        
        //MARK: - 4.Resale Properties View
        propertyForRentView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (195/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForRentView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(propertyForRentView!)
        
        
        
        propertyForRent_Image  = UIImageView(frame:CGRectMake((12/320)*UIScreen.mainScreen().bounds.size.width,(8/568)*UIScreen.mainScreen().bounds.size.height,  (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        
        propertyForRent_Image.image = UIImage(named:"about_icon.png")
        
        
        propertyForRent_btn.addSubview(propertyForRent_Image)
        
        propertyForRent_btn.tag = 3
        
        
        
        propertyForRentLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(12/568)*UIScreen.mainScreen().bounds.size.height,(180/414)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        propertyForRentLabel.textColor = UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        propertyForRentLabel.textAlignment = NSTextAlignment.Left
        propertyForRentLabel.text = "Über uns"
        propertyForRentLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        
        
        
        propertyForRent_btn.addSubview(propertyForRentLabel)
        
        
        //resalePropertiesLabel.backgroundColor = UIColor.greenColor()
        
        propertyForRentLabel.tag = 3
        
        
        propertyForRent_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height)
        
        
        propertyForRent_btn.backgroundColor = UIColor.whiteColor()
        //myEssayButton.textColor = UIColor.blackColor()
        propertyForRent_btn.setTitle ("", forState: UIControlState.Normal)
        propertyForRent_btn.addTarget(self, action:"goToPropertiesOnRentVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        propertyForRentView?.addSubview(propertyForRent_btn)
        
        
        
        
        //propertyForRent_btn.backgroundColor = UIColor.blackColor()
        
        
        propertyForRentView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(248/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForRentView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(propertyForRentView_line)
        
        
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            propertyForRentLabel.font = UIFont.boldSystemFontOfSize(18)
        //            propertyForRentView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            propertyForRentLabel.font = UIFont.boldSystemFontOfSize(13)
        //            propertyForRentView_line.frame.size.height = 1
        //        }
        
        
        
        
        //MARK: - 5.Exchange Properties View
        postPropertyView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (260/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        postPropertyView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(postPropertyView!)
        
        
        
        postProperty_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        postProperty_Image.image = UIImage(named:"log_out_icon.png")
        
        
        postProperty_btn.addSubview(postProperty_Image)
        
        postProperty_btn.tag = 4
        
        
        //exchangePropertyLabel.backgroundColor = UIColor.greenColor()
        
        
        postPropertyLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(12/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //MyPlanLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        postPropertyLabel.textColor = UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        postPropertyLabel.textAlignment = NSTextAlignment.Left
        postPropertyLabel.text = "Ausloggen"
        postPropertyLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        
        
        
        postProperty_btn.addSubview(postPropertyLabel)
        
        
        postPropertyLabel.tag = 4
        
        postProperty_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height)
        
        postProperty_btn.backgroundColor = UIColor.whiteColor()
        //myEssayButton.textColor = UIColor.blackColor()
        postProperty_btn.setTitle ("", forState: UIControlState.Normal)
        postProperty_btn.addTarget(self, action:"goToExchangePropertyVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        postPropertyView?.addSubview(postProperty_btn)
        
        
        
        // postProperty_btn.backgroundColor = UIColor.blackColor()
        
        
        
        postPropertyView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(310/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        postPropertyView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(postPropertyView_line)
        
        
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            postPropertyLabel.font = UIFont.boldSystemFontOfSize(18)
        //            postPropertyView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            postPropertyLabel.font = UIFont.boldSystemFontOfSize(13)
        //            postPropertyView_line.frame.size.height = 1
        //        }
        
        
        
        
        
        
        
        //MARK: - 6.Post Property View
        aboutUsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(320/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        aboutUsView!.backgroundColor = UIColor.whiteColor()
        
        myMenuView.addSubview(aboutUsView!)
        
        
        aboutUs_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        aboutUs_Image.image = UIImage(named:"delete_account_icon.png")
        
        aboutUs_Btn.addSubview(aboutUs_Image)
        
        aboutUs_Btn.tag = 5
        
        
        
        // postPropertyLabel.backgroundColor = UIColor.greenColor()
        
        
        aboutUsLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(12/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        aboutUsLabel.textColor =  UIColor(red: 66.0/255, green: 91.0/255, blue: 163.0/255, alpha: 1.0)
        
        aboutUsLabel.textAlignment = NSTextAlignment.Left
        aboutUsLabel.text = "Konto löschen"
        aboutUsLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        aboutUsLabel.tag = 5
        
        
        aboutUs_Btn.addSubview(aboutUsLabel)
        
        
        aboutUs_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        aboutUs_Btn.backgroundColor = UIColor.whiteColor()
        aboutUs_Btn.setTitle ("", forState: UIControlState.Normal)
        aboutUs_Btn.addTarget(self, action:"goToAboutUsVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        aboutUsView?.addSubview(aboutUs_Btn)
        
        
        // aboutUs_Btn.backgroundColor = UIColor.blackColor()
        
        
        aboutUsView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(370/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        aboutUsView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(aboutUsView_line)
        
        //
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            aboutUsLabel.font = UIFont.boldSystemFontOfSize(18)
        //            aboutUsView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            aboutUsLabel.font = UIFont.boldSystemFontOfSize(13)
        //            aboutUsView_line.frame.size.height = 1
        //        }
        //
        //        //MARK: - 7.Contact Us View
        //        feedBackView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(245/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        //
        //        feedBackView!.backgroundColor = UIColor.whiteColor()
        //
        //        myMenuView.addSubview(feedBackView!)
        //
        //        feedBack_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        feedBack_btn.backgroundColor = UIColor.whiteColor()
        //        //myEssayButton.textColor = UIColor.blackColor()
        //        feedBack_btn.setTitle ("", forState: UIControlState.Normal)
        //        feedBack_btn.addTarget(self, action:"goToFeedBackVC:",forControlEvents: UIControlEvents.TouchUpInside)
        //
        //        feedBack_btn.tag = 6
        //
        //
        //
        //        //feedBack_btn.backgroundColor = UIColor.blackColor()
        //
        //
        //        feedBack_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        //
        //        feedBack_Image.image = UIImage(named:"i7.png")
        //
        //        feedBack_btn.addSubview(feedBack_Image)
        //
        //
        //        // contactUsView_btn.backgroundColor = UIColor.whiteColor()
        //
        //        feedBackLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(2/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        feedBackLabel.textColor = UIColor.blackColor()
        //        feedBackLabel.textAlignment = NSTextAlignment.Left
        //        feedBackLabel.text = "POST PROPERTY"
        //
        //
        //        feedBack_btn.addSubview(feedBackLabel)
        //
        //
        //        feedBackLabel.tag = 6
        //
        //        //contactUsLabel.backgroundColor = UIColor.greenColor()
        //
        //        feedBackView?.addSubview(feedBack_btn)
        //
        //
        //        feedBackView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(282/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        feedBackView_line.backgroundColor = UIColor.grayColor()
        //
        //        myMenuView.addSubview(feedBackView_line)
        //
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            feedBackLabel.font = UIFont.boldSystemFontOfSize(18)
        //            feedBackView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            feedBackLabel.font = UIFont.boldSystemFontOfSize(13)
        //            feedBackView_line.frame.size.height = 1
        //        }
        //
        //
        //
        //
        //
        //
        //
        //
        //        //MARK: - 7.Contact Us View
        //        contactUSView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(285/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (40/568)*UIScreen.mainScreen().bounds.size.height))
        //
        //        contactUSView!.backgroundColor = UIColor.whiteColor()
        //
        //        myMenuView.addSubview(contactUSView!)
        //
        //        contactUS_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        contactUS_btn.backgroundColor = UIColor.whiteColor()
        //        //myEssayButton.textColor = UIColor.blackColor()
        //        contactUS_btn.setTitle ("", forState: UIControlState.Normal)
        //        contactUS_btn.addTarget(self, action:"goToCOntactsVC:",forControlEvents: UIControlEvents.TouchUpInside)
        //
        //        contactUS_btn.tag = 7
        //
        //
        //
        //
        //
        //
        //        contactUS_Image  = UIImageView(frame:CGRectMake((15/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height, (18/568)*UIScreen.mainScreen().bounds.size.height))
        //
        //        contactUS_Image.image = UIImage(named:"i7.png")
        //
        //        contactUS_btn.addSubview(contactUS_Image)
        //
        //
        //        // contactUsView_btn.backgroundColor = UIColor.whiteColor()
        //
        //
        //
        //        contactUSLabel.frame = CGRectMake((45/320)*UIScreen.mainScreen().bounds.size.width,(2/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        contactUSLabel.textColor = UIColor.blackColor()
        //        contactUSLabel.textAlignment = NSTextAlignment.Left
        //        contactUSLabel.text = "CONTACT US"
        //
        //
        //        contactUS_btn.addSubview(contactUSLabel)
        //
        //
        //        contactUSLabel.tag = 7
        //
        //        //contactUsLabel.backgroundColor = UIColor.greenColor()
        //
        //        contactUSView?.addSubview(contactUS_btn)
        //
        //
        //        contactUSView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(323/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        //
        //        contactUSView_line.backgroundColor = UIColor.grayColor()
        //
        //        myMenuView.addSubview(contactUSView_line)
        //
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        //        {
        //            contactUSLabel.font = UIFont.boldSystemFontOfSize(18)
        //            contactUSView_line.frame.size.height = 1
        //        }
        //        else
        //        {
        //            contactUSLabel.font = UIFont.boldSystemFontOfSize(13)
        //            contactUSView_line.frame.size.height = 1
        //        }
        //
        //
        
        
        
    }
    
    
    func closeMenuBtnClicked(sender: UIButton)
    {
        print("menu btn pressed")
        sender.hidden = true
        
        x = -myMenuView.frame.size.width
        
        UIView.animateWithDuration(0.5, animations: {
            
            // self.navBar.frame.origin.x = self.navx
            myMenuView.frame.origin.x = -myMenuView.frame.size.width
            
        })
        
    }
    
    
    
    func goTDashBoardVC(sender: UIButton!)
    {
        
    }
    //
    //
    //
    //
    func goToAdvancedSearchVC(sender: UIButton!)
    {
        
        //       // highLightBtn(sender)
        //        closeMenuBtn.hidden = true
        //
        //        print("dashboard button clicked")
        //
        //let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //
        let contact = currentVC.storyboard!.instantiateViewControllerWithIdentifier("ContactViewController") as! ContactViewController
        //
        //        //let navigationItem = UINavigationItem()
        //       // navBarTitle = "HOME"
        //
        //        // barButtonCheck = "2"
        //
        //       // UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        //
        //        //urlLink = "http://smartmoverealty.in/?mobile=true"
        //
        currentVC.navigationController?.pushViewController(contact, animated: true)
        
        
        
    }
    //
    //
    //
    //
    func goToPropertiesForSaleVC(sender: UIButton!)
    {
        
        //       // highLightBtn(sender)
        //        closeMenuBtn.hidden = true
        //
        //        print("advance search view button clicked")
        //
        //
        //
        //        //navBarTitle = "ABOUT"
        //
        //       // UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        //
        //
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //
        let Imprint = currentVC.storyboard!.instantiateViewControllerWithIdentifier("ImprintViewController") as! ImprintViewController
        //
        //        //urlLink = "http://smartmoverealty.in/about/?mobile=true"
        //
        //        //barButtonCheck = "1"
        //
        currentVC.navigationController?.pushViewController(Imprint, animated: true)
    }
    
    func goToPropertiesOnRentVC(sender: UIButton!)
    {
        //       // highLightBtn(sender)
        //        closeMenuBtn.hidden = true
        //
        //
        //
        //        print("property for sale button clicked")
        //
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //
        let AboutUs = currentVC.storyboard!.instantiateViewControllerWithIdentifier("AboutUsViewController") as! AboutUsViewController
        //
        //        //barButtonCheck = "1"
        //
        //        //saleCheck = "sale_menu"
        //
        //
        //       // navBarTitle = "PROJECTS"
        //
        //       // UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        //
        //       // urlLink = "http://smartmoverealty.in/projects/?mobile=true"
        //
        currentVC.navigationController?.pushViewController(AboutUs, animated: true)
        
        
        
    }
    
    
    func goToExchangePropertyVC(sender: UIButton!)
    {
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to logout", preferredStyle: .Alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //
            let AboutUs = currentVC.storyboard!.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
            currentVC.navigationController?.pushViewController(AboutUs, animated: false)
            
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        
        // Present the controller
        currentVC.presentViewController(alertController, animated: true, completion: nil)
        
        
        
        
    }
    
    func goToAboutUsVC(sender: UIButton!)
    {
        
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete your account", preferredStyle: .Alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //
            let AboutUs = currentVC.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
            currentVC.navigationController?.pushViewController(AboutUs, animated: false)
            
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        currentVC.presentViewController(alertController, animated: true, completion: nil)
        
        
    }
    
    
    
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    

}

