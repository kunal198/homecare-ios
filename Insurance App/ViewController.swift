//
//  ViewController.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
    }

    @IBAction func registerbtn(sender: AnyObject)
    {
        
        let Register = self.storyboard!.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        
       // SignIn.reg = "REGISTRIEREN"
        
        self.navigationController?.pushViewController(Register, animated: true)

    }

    
    
    
    @IBAction func signinbtn(sender: AnyObject)
    {
        let SignIn = self.storyboard!.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        
        self.navigationController?.pushViewController(SignIn, animated: true)

    }
  
    
    
    @IBAction func FAQbtn(sender: AnyObject)
    {
        let Faq = self.storyboard!.instantiateViewControllerWithIdentifier("FaqViewController") as! FaqViewController
        
        self.navigationController?.pushViewController(Faq, animated: true)

        
    }
    
    @IBAction func contact(sender: AnyObject)
    {
        let Contact = self.storyboard!.instantiateViewControllerWithIdentifier("ContactViewController") as! ContactViewController
        
        self.navigationController?.pushViewController(Contact, animated: true)
        
 
    }
    
    
    
}

