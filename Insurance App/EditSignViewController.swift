//
//  EditSignViewController.swift
//  Insurance App
//
//  Created by brst on 18/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class EditSignViewController: UIViewController {
      var fullpath = NSString()
      var vedioURL = NSURL()
    
    @IBOutlet var drawSignatureView: YPDrawSignatureView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }


    @IBAction func continuebtn(sender: AnyObject)
    {
       self.navigationController?.popViewControllerAnimated(true)
        
        let signatureImage = self.drawSignatureView.getSignature()
        
        
        UIImageWriteToSavedPhotosAlbum(signatureImage, nil, nil, nil)
        
        self.drawSignatureView.clearSignature()
        
        
    }
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

}
