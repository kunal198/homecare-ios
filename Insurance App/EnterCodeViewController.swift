//
//  EnterCodeViewController.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class EnterCodeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var selectbtn: UIImageView!
    @IBOutlet var blankbtn: UIButton!
    @IBOutlet var codetext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        codetext.delegate = self
        self.selectbtn.hidden = true
        self.blankbtn.hidden = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    @IBAction func backbtn(sender: AnyObject)
    
    {
       self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func `continue`(sender: AnyObject)
    {
        if (self.codetext.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Message"
            alert.message = "Enter the Verification Code"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
        else if (self.codetext.text!.characters.count <= 4)
        {
            
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Enter correct Verification Code"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }

        else
        {
        
        
        let Insurance = self.storyboard!.instantiateViewControllerWithIdentifier("InsuranceViewController") as! InsuranceViewController
        
        self.navigationController?.pushViewController(Insurance, animated: true)
        }
    }
   
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.codetext .resignFirstResponder()
    }
    
    
    @IBAction func refresh(sender: AnyObject)
    {
        
        
        
    }
    
    
    @IBAction func code(sender: AnyObject)
    {
        
        self.codetext.resignFirstResponder()
      //textFieldDidBeginEditing
        
    }
     func textFieldDidBeginEditing(textField: UITextField) {
        if codetext.tag == 1 {
            self.view.frame = CGRectMake(self.view.frame.origin.x, -100, self.view.frame.size.width, self.view.frame.size.height)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if codetext.tag == 1 {
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)
        }
    }

}
