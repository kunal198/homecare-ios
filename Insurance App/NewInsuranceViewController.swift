//
//  NewInsuranceViewController.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class NewInsuranceViewController: UIViewController {
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    @IBOutlet var companyname: UIImageView!
    
    @IBOutlet var ischecked: UILabel!
    
    @IBOutlet var namecompany: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        myMenuView.hidden = true
        currentVC = self
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        // Do any additional setup after loading the view.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func back(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func `continue`(sender: AnyObject)
    {
        let NewInsurance = self.storyboard!.instantiateViewControllerWithIdentifier("SelectyourInsuranceViewController") as! SelectyourInsuranceViewController
        
        self.navigationController?.pushViewController(NewInsurance, animated: false)

    }

    @IBAction func addbtn(sender: AnyObject)
    {
        myMenuView.hidden = true
       self.navigationController?.popViewControllerAnimated(true)
    }
   
    
    
    
    @IBAction func menubtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            x = -myMenuView.frame.size.width
            navx = 0
            closeMenuBtn.hidden = true
        }
        
        
        UIView.animateWithDuration(0.5, animations: {
            
            // self.navBar.frame.origin.x = self.navx
            myMenuView.frame.origin.x = self.x
            
        })

    }

}
